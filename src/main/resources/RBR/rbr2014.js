	var category = new Array();
	category[0] = "nutrition";
	category[1] = "education";
	category[2] = "development";
	category[3] = "physical";
	category[4] = "immunization";
	category[5] = "parentConcerns";
	category[6] = "problemsPlans";
	category[7] = "period_title";
	
	var NUTRITION = 0;
	var EDUCATION = 1
	var DEVELOPMENT = 2;
	var PHYSICAL = 3;
	var IMMUNIZATION = 4;
	
	var current_guide = 1;
	var current_visit = "1w";
	var current_category = NUTRITION;
	var current_edit_list = null;
	var previous_resources = null;
	var default_action = null;
	var statusGraph = false;

	var today = new Date();
	var todayDate = today.getFullYear() + "-" + ("0" + (today.getMonth() + 1)).slice(-2) + "-" + ("0" + today.getDate()).slice(-2);


	function start() {
		set_window_size();
		hide_resources_box();
		hide_referral_list();
		hide_save();
		set_default_action();
		set_lastFormValues(); //must go before any settings to the form fields
		set_birthday_set_current(); //must go before switch_show_all(), set var current_guide
		switch_show_all(false); //must go after set_birthday_set_current(), use var current_guide
		set_nddsNotAttained();
		show_genderMF();
		load_weights();
		loadAllCommunityRes();
		loadComments();
		load_riskfactors_familyhistory();
	}


	
	function set_window_size() {
		top.window.moveTo(0,0);
		if (document.all) {
			top.window.resizeTo(1200,screen.availHeight);
		}
		else if (document.layers||document.getElementById) {
			if (top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth){
				top.window.outerHeight = screen.availHeight;
				top.window.outerWidth = 1200;
			}
		}
	}
	
	function set_default_action() {
		default_action = document.formRourke.action;
	}
	
	function set_birthday_set_current() {
		if ($("birthday").value=="") {
			$("birthday").value = $("birthdayY").value + "-" + $("birthdayM").value + "-" + $("birthdayD").value;
		}
		var diffY = today.getFullYear() - $("birthdayY").value;
		var diffM = today.getMonth() + 1 - $("birthdayM").value + diffY*12;
		var diffD = today.getDate() - $("birthdayD").value;
		
		if ($("current_guide").value!="") {
			current_guide = $("current_guide").value;
			return;
		}
		
		if (diffM > 18 || (diffM==18 && diffD>=0)) {
			current_guide = 4;
			
			if (diffM > 48 || (diffM==48 && diffD>=0)) {
				current_visit = "4y";
			}
			else if (diffM > 24 || (diffM==24 && diffD>=0)) {
				current_visit = "2y";
			}
			else {
				current_visit = "18m";
			}
		}
		else if (diffM > 9 || (diffM==9 && diffD>=0)) {
			current_guide = 3;
			
			if (diffM > 15 || (diffM==15 && diffD>=0)) {
				current_visit = "15m";
			}
			else if (diffM > 12 || (diffM==12 && diffD>=0)) {
				current_visit = "12m";
			}
			else {
				current_visit = "9m";
			}
		}
		else if (diffM > 2 || (diffM==2 && diffD>=0)) {
			current_guide = 2;
			
			if (diffM > 6 || (diffM==6 && diffD>=0)) {
				current_visit = "6m";
			}
			else if (diffM > 4 || (diffM==4 && diffD>=0)) {
				current_visit = "4m";
			}
			else {
				current_visit = "2m";
			}
		}
		else {
			// current_guide = 1 (initial)
			
			if (diffM > 1 || (diffM==1 && diffD>=0)) {
				current_visit = "1m";
			}
			else if (diffD>=14) {
				current_visit = "2w";
			}
			else {
				// current_visit = "1w" (initial)
			}
		}
		
		if ($("visit_date_"+current_visit).value=="") {
			$("visit_date_"+current_visit).value = todayDate;
			displayDate("visit_date_"+current_visit);
		}
	}
	
	function show_genderMF() {
		if ($("gender").value=="") return;
		
		$("genderM").checked = false;
		$("genderF").checked = false;
		if ($("gender").value=="M") $("genderM").checked = true;
		if ($("gender").value=="F") $("genderF").checked = true;
	}
	
	function legend_on(legendName) {
		if (legendName.substr(0,11)=="nutritionYN") {
			$(legendName).update("&nbsp; <u>Y</u>es = Provided<br/>&nbsp; <u>N</u>o = Not provided");
		}
		else if (legendName.substr(0,9)=="nutrition" || legendName.substr(0,12)=="immunization") {
			$(legendName).update("&nbsp; &#10003; = No concerns<br/>&nbsp; &#10005; = If concerns");
		}
		else if (legendName.substr(0,9)=="education") {
			$(legendName).update("&nbsp; &#10003; = Discussed and no concerns<br/>&nbsp; &#10005; = Has concerns");
		}
		else if (legendName.substr(0,11)=="development") {
			$(legendName).update("&nbsp; &#10003; = Attained<br/>&nbsp; &#10005; = Not attained");
		}
		else if (legendName.substr(0,8)=="physical") {
			$(legendName).update("&nbsp; &#10003; = Normal<br/>&nbsp; &#10005; = Abnormal");
		}
	}
	
	function legend_off(legendName) {
		$(legendName).update("");
	}

	function show_guide(guide_id) {
		hide_all_guides();
		
		current_guide = guide_id;
		$$(".guide" + guide_id).each(
			function (e) {
				e.show();
			}
		);
		change_category(current_category);
	}

	function hide_all_guides() {
		for (var i=1; i<5; i++) {
			$$(".guide" + i).each(
				function (e) {
					e.hide();
				}
			);
		}
	}

	function show_all_guides() {
		for (var i=1; i<5; i++) {
			$$(".guide" + i).each(
				function (e) {
					e.show();
				}
			);
		}
	}
	
	function change_category(category_id) {
		hide_all_categories();
		
		current_category = category_id;
		show_current_category();
		
		hide_resources_box();
	}
	
	function show_current_category() {
		$$("." + category[current_category] + ".guide" + current_guide).each(
			function (e) {
				e.show();
			}
		);
		$("btn_"+category[current_category]).hide();
		$("sel_"+category[current_category]).show();
		
		for (var i=5; i<8; i++) { //show parentConcerns, problemsPlans & period_titles
			$$("." + category[i] + ".guide" + current_guide).each(
				function (e) {
					e.show();
				}
			);
		}
	}
	
	function hide_all_categories() {
		for (var i=0; i<8; i++) {
			$$("." + category[i]).each(
				function (e) {
					e.hide();
				}
			);
		}
		$$(".category_btn").each(
			function (e) {
				e.show();
			}
		);
		$$(".selected_btn").each(
			function (e) {
				e.hide();
			}
		);
	}
	
	function to_prevention() {
		var demographicNo = $("demographic_no").value;
		window.open("../oscarPrevention/index.jsp?demographic_no="+demographicNo);
	}
	
	function switch_show_all(show_all) {
		if (show_all) {
			$("category_btns").hide();
			$("view_all_back_btn").show();
			hide_all_guides();
		}
		else {
			$("category_btns").show();
			$("view_all_back_btn").hide();
			show_guide(current_guide);
			showNormalViews(true);
			showShowAlls(false);
		}
		$$(".category_title").each(
			function (e) {
				if (show_all) e.show();
				else e.hide();
			}
		);
		$$(".guide_btn").each(
			function (e) {
				if (show_all) e.hide();
				else e.show();
			}
		);
		$$(".view_all_btn").each(
			function (e) {
				if (show_all) e.hide();
				else e.show();
			}
		);
	}
	
	function showNormalViews(show) {
		$$(".normalView").each(
			function (e) {
				if (show) e.show();
				else e.hide();
			}
		);
	}
	
	function showShowAlls(show) {
		$$(".showAll").each(
			function (e) {
				if (show) e.show();
				else e.hide();
			}
		);
	}
	
	function show_all(period) {
		switch_show_all(true);
		hide_all_categories();
		
		$$("."+period).each(
			function (e) {
				e.show();
			}
		);
		
		var content = $("parentConcerns"+period).value;
		if (content.trim()!="") {
			$("parentConcernsShowAll"+period).innerHTML = content.replace(/\n/g, "<br/>");
			$("parentConcernsShowAll"+period).show();
			$("parentConcerns"+period).hide();
		}
		content = $("problemsPlans"+period).value;
		if (content.trim()!="") {
			$("problemsPlansShowAll"+period).innerHTML = content.replace(/\n/g, "<br/>");
			$("problemsPlansShowAll"+period).show();
			$("problemsPlans"+period).hide();
		}
	}
	
	function show_resources(resources_id) {
		hide_resources_box();
		
		if (previous_resources!=null) {
			$(previous_resources).setStyle({border:"none"});
		}
		$(resources_id).setStyle({border:"solid 1px #c00"});
		previous_resources = resources_id;
		
		show_resources_page(resources_id);
		$("resources_box").show();
		$("hanging_box").show();
		to_anchor(resources_id);
	}
	
	function show_resources_page(resources_id) {
		for (var i=1; i<4; i++) {
			$("resources"+i).hide();
		}
		$("resources"+resources_id.charAt(0)).show();
	}
	
	function hide_resources_box() {
		$("resources_box").hide();
		$("hanging_box").hide();
		hideCommunityRes();
		hide_list_edit();
	}
	
	function checkGestationalAge(gAge) {
		if (gAge.trim()=="") return true;
		
		if (gAge<100) {
			alert("Please enter Gestational Age in DAYS");
			setTimeout(function() { $('gestational_age').focus(); }, 10);
			return false;
		}
	}
	
	function checkCopyWeight(wtObjShow) {
		var baseId = wtObjShow.id.substring(0, wtObjShow.id.length-4); //extract weight id from "...Show"
		if (wtObjShow.value.trim()=="") {
			$(baseId).value = "";
			return;
		}
		
		if(wtObjShow.value.trim()!="" && validationNumberOnly(wtObjShow)){
					
			if (wtObjShow.value<100) {
				wtObjShow.value = wtObjShow.value*1000+0.5;
				wtObjShow.value = parseInt(wtObjShow.value);
			}
			$(baseId).value = wtObjShow.value;
			
		}else{
			validateNumMsg();
		}
	}
	
	function validateWeight(weight){

		if(validationNumberOnly(weight) ){
			checkCopyWeight(weight);
			showVisitsWtLoss();
		}else{
			validateNumMsg();
		}
	}
	
	function click_one(one, two) {
		if (one.checked) {
			two.checked = false;
		}
	}
	
	function checkAllYes(classId) {
		var valueTrue = $(classId).value==0;
		
		$$(classId+" input[type=checkbox]").each(
			function (e) {
				var id = e.id;
				if (id.substr(id.length-2)=="_o") e.checked = valueTrue;
				else if (id.substr(id.length-2)=="_x") e.checked = false;
			}
		)
		if (valueTrue) $(classId).value = 1;
		else $(classId).value = 0;
	}
	
	function to_anchor(anchor) {
		var href = window.location.href;
		var cutPt = href.lastIndexOf("#");
		if (cutPt > 0) {
			href = href.substring(0, cutPt);
		}
		window.location.href = href + "#" + anchor;
	}
	
	function hide_list_edit() {
		$("list_edit_box").hide();
	}

	function call_list_entry(listbox) {
		var oscarCount = $(listbox.id+"_count").value;
		if (listbox.selectedIndex<oscarCount) {
			show_list_entry(listbox);
		} else {
			add_edit_list_entry(listbox);
		}
	}
	
	function add_edit_list_entry(listbox) {
		current_edit_list = listbox;
		
		var addEntry = false;
		if ($(listbox.id+"_count").value==listbox.length) {
			addEntry = true;
			listbox.selectedIndex = listbox.length;
		} else {
			//edit entry
			listbox.selectedIndex = listbox.length-1;
		}
		
		$("listEditTitle").update(listbox.title);
		$("listEditEntry").readOnly = false;
		$("list_update_btn").show();
		if (addEntry) {
			$("listEditEntry").value = "";
			$("list_delete_btn").hide();
		} else {
			$("listEditEntry").value = listbox.value;
			$("list_delete_btn").show();
		}
		
		$("list_edit_box").show();
		$("listEditEntry").focus();
	}
	
	function show_list_entry(listbox) {
		current_edit_list = listbox;
		
		$("listEditTitle").update(listbox.title);
		$("listEditEntry").value = listbox.value;
		$("listEditEntry").readOnly = true;
		$("list_update_btn").hide();
		$("list_delete_btn").hide();
		
		$("list_edit_box").show();
	}
	
	function update_list_entry(entry) {
		entry = entry.trim();
		if (entry.length>0) {
			if (current_edit_list.selectedIndex==-1) { //No option selected, add new entry
				current_edit_list.options.add(new Option(entry, entry));
			}
			else {
				current_edit_list.options[current_edit_list.selectedIndex].text = entry;
				current_edit_list.options[current_edit_list.selectedIndex].value = entry;
			}
			save_selects();
		}
		hide_list_edit();
	}
	
	function delete_list_entry() {
		current_edit_list.options.remove(current_edit_list.selectedIndex);
		$(current_edit_list.id + "Save").value = "";
		hide_list_edit();
	}
	
	function save_selects() {
		var saveObj = $(current_edit_list.id + "Save");
		var saveIndex = $(current_edit_list.id + "_count").value;
		saveObj.value = current_edit_list.options[saveIndex].value;
	}
	
	function load_riskfactors_familyhistory() {
		var rfArray = new Array("riskFactors","familyHistory");

		//load familyHistory data from Rourke2009 form
		if ($("familyHistoryTransfer").value!="") {
			var transferVal = $("familyHistoryTransfer").value;
			$("familyHistory").options.add(new Option(transferVal, transferVal));
			$("familyHistory_count").value = parseInt($("familyHistory_count").value)+1;
		}
		
		for (var i=0; i<rfArray.length; i++) {
			//load data from Oscar echart
			var dataVal = $(rfArray[i]+"Load").value;
			var dataJson = null;
			try {
				dataJson = JSON.parse(dataVal);
			} catch (err) {
				//dataVal is not a valid JSON. Check riskFactorsLoad/familyHistoryLoad for the value.
			}

			if (dataJson!=null) {
				for (var j=0; j<dataJson.length; j++) {
					var entry = dataJson[j]["note"];
					$(rfArray[i]).options.add(new Option(entry, entry));
				}
				if ($(rfArray[i]+"_count").value==0) $(rfArray[i]+"_count").value = dataJson.length;
			}
			$(rfArray[i]+"Load").value = dataVal.replace(/"/g, "&quot;");
			
			//load data from previous draft or saved form (for viewing past forms)
			var savedEntry = $(rfArray[i]+"Save").value;
			if (savedEntry!="") {
				$(rfArray[i]).options.add(new Option(savedEntry, savedEntry));
			}
		}
   }

	var periods = new Array("1w","2w","1m","2m","4m","6m","9m","12m","15m","18m","2y","4y");
	var comResPeriod = null;

	function loadAllCommunityRes() {
		for (var i=0; i<periods.length; i++) {
			loadCommunityRes(periods[i]);
			saveCommunityRes();
		}
	}
	
	function loadCommunityRes(period) {
		comResPeriod = period;
		clearCommunityRes();
		
		var comResChecked = $("problemsPlans"+period+"Res").value.trim().split(",");
		if (comResChecked[0].trim()!="") {
			for (var i=0; i<comResChecked.length; i++) {
				$("com_res_"+comResChecked[i]).checked = true;
			}
		} 
	}
	
	function showCommunityRes(period) {
		loadCommunityRes(period);
		
		$("community_res_title").innerHTML = "Community Resources (" + period + ")";
		$("referral_source_period").value = period;
		$("community_res_box").show();
		$("com_res_AIS").focus();
	}
	
	function clearCommunityRes() {
		$$(".com_res").each(
			function (e) {
				e.checked = false;
			}
		);
	}
	
	function saveCommunityRes() {
		var comResChecked = null;
		var comResList = "";
		$$(".com_res").each(
			function (e) {
				if (e.checked) {
					if (comResChecked==null) comResChecked = e.id.substring(8);  
					comResChecked += "," + e.id.substring(8);
					comResList += "<li>" + e.next().next().innerHTML + "</li>";
				}
			}
		);
		$("problemsPlans"+comResPeriod+"Res").value = comResChecked;
		
		if (comResList!="") comResList = "<ul>" + comResList + "</ul>";
		$("communityResourcesBox"+comResPeriod).innerHTML = comResList;
		
		if (comResChecked!=null) $("commResBtn18m").innerHTML = "Edit";
		else $("commResBtn"+comResPeriod).innerHTML = "Add";
		
		hideCommunityRes();
	}
	
	function hideCommunityRes() {
		$("community_res_box").hide();
	}

	var commentIds = new Array("p1_nutrition1w","p1_nutrition2w","p1_nutrition1m","p2_nutrition2m","p2_nutrition4m","p2_nutrition6m","p3_nutrition9m","p3_nutrition12m","p3_nutrition15m","p4_nutrition18m","p4_nutrition24m","p4_nutrition48m","p1_education","p2_education","p3_education","p4_education18m","p4_education48m","p1_development1w","p1_development2w","p1_development1m","p2_development2m","p2_development4m","p2_development6m","p3_development9m","p3_development12m","p3_development15m","p4_development18m","p4_development24m","p4_development36m","p4_development48m","p4_development60m","p1_physical1w","p1_physical2w","p1_physical1m","p2_physical2m","p2_physical4m","p2_physical6m","p3_physical9m","p3_physical12m","p3_physical15m","p4_physical18m","p4_physical24m","p4_physical48m","p1_immunization1w","p1_immunization2w","p1_immunization1m","p2_immunization2m","p2_immunization4m","p2_immunization6m","p3_immunization9m","p3_immunization15m","p4_immunization18m","p4_immunization2y","p4_immunization4y");
	
	function loadComments() {
		for (var i=0; i<commentIds.length; i++) {
			if ($(commentIds[i]).value.trim() == "") {
				$(commentIds[i]).hide();
			} else {
				$("btn_"+commentIds[i]).hide();
			}
		}
	}
	
	function openComments(commentBtn) {
		var commentId = commentBtn.id.substring(4);
		commentBtn.hide();
		$(commentId).show();
		$(commentId).focus();
	}
	
	function set_lastFormValues() {
		var lastValuesJson = null;
		
		if($("lastValuesAll").value!=""){ //only try if not empty lastValuesAll
			
			try {
				lastValuesJson = JSON.parse($("lastValuesAll").value);
			} catch (err) {
				//try to resolve by removing json values from this json value
				var lastValuesAll = $("lastValuesAll").value;
				var fieldValueTag = "\"fieldvalue\":\"[{";
				var fieldValueEndTag = "}]\"";
				var fieldValueX = -1;
				do {
					fieldValueX = lastValuesAll.indexOf(fieldValueTag, fieldValueX);
					if (fieldValueX >= 0) {
						var fieldValueEndX = lastValuesAll.indexOf(fieldValueEndTag, fieldValueX);
						lastValuesAll = lastValuesAll.substring(0, fieldValueX+fieldValueTag.length) + lastValuesAll.substring(fieldValueEndX);
						fieldValueX += fieldValueTag.length;
					}
				} while (fieldValueX >= 0);
	
				try {
					lastValuesJson = JSON.parse(lastValuesAll);
				} catch (err1) {
					//lastValuesAll is not a valid JSON
				}
			}
			
		if (lastValuesJson!=null) {
			for (var i=0; i<lastValuesJson.length; i++) {
				var field = lastValuesJson[i]["fieldname"];
				if(field.substr(field.length - 7)=="_yes_no"){
					document.getElementsByName(field)[0].value=lastValuesJson[i]["fieldvalue"];
				}	
				
				if (field!=null) field = field.trim();
				if ($(field)==undefined) continue;
				if ($(field).className.indexOf("lastFormValue")<0) continue;
				
				if ($(field).type=="checkbox") {
					if (lastValuesJson[i]["fieldvalue"]=="on") $(field).checked = true;
				} else {
					$(field).value = lastValuesJson[i]["fieldvalue"];
				}
			}
		}
		
		displayAllVisitDates();
		showVisitsWtLoss();
		}//only try if not empty lastValuesAll
	}
	
	function set_nddsNotAttained() {
		var nddsValue = {};
		nddsValue["identifypicturesN"] = 1;
		nddsValue["usegesturesN"] = 2;
		nddsValue["followdirectionsN"] = 3;
		nddsValue["make4consonantsN"] = 4;
		nddsValue["point3bodypartsN"] = 5;
		nddsValue["say20wordsN"] = 6;
		nddsValue["holdcupdrinkN"] = 7;
		nddsValue["eatfingerfoodN"] = 8;
		nddsValue["helpwithdressingN"] = 9;
		nddsValue["walkupstairsN"] = 10;
		nddsValue["walkaloneN"] = 11;
		nddsValue["squatpickstandN"] = 12;
		nddsValue["pushpullwalkN"] = 13;
		nddsValue["stack3blocksN"] = 14;
		nddsValue["showaffectionN"] = 15;
		nddsValue["pointtoshowN"] = 16;
		nddsValue["lookwhentalkN"] = 17;

		var nddsNotAttained = new Array();
		var nddsJson = null;
		try {
			nddsJson = JSON.parse($("nddsValuesAll").value);
		} catch (err) {
			//nddsValuesAll is not a valid JSON
		}
		if (nddsJson!=null) {
			var ii = 0;
			for (var i=0; i<nddsJson.length; i++) {
				if (nddsJson[i]["fieldname"]=="today") {
					$("ndds_today").value = nddsJson[i]["fieldvalue"];
				}
				else if (nddsJson[i]["fieldname"]=="subject") {
					$("ndds_subject").value = nddsJson[i]["fieldvalue"];
				}
				else if (nddsValue[nddsJson[i]["fieldname"]]!=undefined) {
					if (nddsJson[i]["fieldvalue"]=="on") {
						nddsNotAttained[ii++] = nddsValue[nddsJson[i]["fieldname"]];
						//sort nddsNotAttained
						for (var j=nddsNotAttained.length-1; j>0; j--) {
							if (nddsNotAttained[j]<nddsNotAttained[j-1]) {
								var nddsTmp = nddsNotAttained[j];
								nddsNotAttained[j] = nddsNotAttained[j-1];
								nddsNotAttained[j-1] = nddsTmp; 
							}
							else break;
						}
					}
				}
			}
			$("nddsNotAttained").value = nddsNotAttained.toString();
		}
	}

	
	function hide_save() {
		$("subject").hide();
		$("CloseButton").hide();
		
		var subject = $("subject").value;
		if (subject.substr(0,9)=="Submitted") {
			$("SaveDraft").hide();
			$("SubmitButton").hide();
			$("subject").show();
			$("CloseButton").show();
			releaseDirtyFlag();
		}
	}
	
	function draftSave() {
		$("subject").value = "Draft copy";
		resumeNonGraphDefaults();
		releaseDirtyFlag();
	}
	
	function signSave() {
		if(validateAllNumbers()){
			if($("formRourke").checkValidity()) {	
			var yes = confirm("Are you sure you want to sign & save? You cannot alter this form afterwards.");
			if (!yes) return;
			
			$("subject").value = "Submitted " + todayDate + ", creator: " + $("frm_creator").value;
			$("frm_signSaved").value = "Yes";
			$("riskFactorsUpload").value = $("riskFactorsSave").value; 
			$("familyHistoryUpload").value = $("familyHistorySave").value;
	
			resumeNonGraphDefaults();
			releaseDirtyFlag();
			
			//based on age (in months), and latest visit..create prompt
			deferred = jQuery.post(jQuery("#formRourke").attr('action'), jQuery("#formRourke").serialize());
	
	 	    deferred.success(function () {
	 	    	runPrompts();          	      
	 	    });
	
	 	    deferred.error(function () {
	 	        alert('An error occured while submitting the form');
	 	    });
			}else{
				validateNumMsg();
			}
		}
	}

	function validateAllNumbers(){
		
		//var numberFields = ["gestational_age","birth_length","birth_wtShow","head_circ","discharge_wtShow"];
		var numberFields = ["birth_length","birth_wtShow","head_circ","discharge_wtShow"];
		
		for (var i = 0; i < numberFields.length; i++){
			if( !validationNumberOnly(document.getElementById(numberFields[i])) ){
				validateNumMsg();
				return false;
				break;
			}
		}
		
		//height_1w
		for (var i = 0; i < periods.length; i++){
			if( !validationNumberOnly(document.getElementById("height_" + periods[i])) ){
				validateNumMsg();
				return false;
				break;
			}
		}		
		
		//weight_1wShow
		for (var i = 0; i < periods.length; i++){
			if( !validationNumberOnly(document.getElementById("weight_" + periods[i] + "Show")) ){
				validateNumMsg();
				return false;
				break;
			}
		}
		
		//headcirc_1w
		for (var i = 0; i < periods.length-1; i++){
			if( !validationNumberOnly(document.getElementById("headcirc_" + periods[i])) ){
				validateNumMsg();
				return false;
				break;
			}
		}
		
			 
		return true;
	}
	
	function validateNumMsg(){
		window.scrollTo(0, 0);
		alert("Please enter only numbers.");
	}
	
	function validationNumberOnly(e){
		    value = e.value;	    
		    if(value!=""){
			    var regex = new RegExp(/^([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$/);
			    if(!value.match(regex)) {
			    	e.focus();
			    	return false;
			    }
		    }
		    return true;
	}
	
	function checkNDDSMissing(period) {
		//TODO: check the subject too..don't include drafts

		var nVisitDate = jQuery("#ndds_today_"+period).val();
		if(nVisitDate == null || nVisitDate == '') {
			return true;
		} 
		return false;
	}
	
	function runPrompts() {
		var disableBornPrompts=false;
		jQuery.ajax('../ws/rs/providerService/settings/get',{async:false,method:'GET',dataType:'json',success:function(data){
			if(data != null && data.content != null) {
				if(data.content[0].disableBornPrompts == true) {
					disableBornPrompts=true;
				}
			}
		}});
		
		console.log('property disableBornPrompts=' + disableBornPrompts);
		
		releaseDirtyFlag();
		
		if(disableBornPrompts === true) {
			window.close();
		}
		
		var demoId=jQuery("#demographic_no").val();
		var age = parseInt(jQuery("#age_in_months").val());
		
		var filled1m = jQuery("#ndds_filled_1m").val();
		var filled2m = jQuery("#ndds_filled_2m").val();
		
		//get latest visit
		var latestPeriod = null;
		for(var i=0;i<periods.length;i++) {
			var dateVal = jQuery("#visit_date_"+periods[i]).val();
			
			if(dateVal != null && dateVal.length > 0) {
				latestPeriod = periods[i];
			}
		}
		
		var eformToForwardTo = null;
		
		if(latestPeriod == '1m' && (checkNDDSMissing('1m') || filled1m != 'Yes') ){
			eformToForwardTo = "NDDS+1%262+Months";
		} else if(latestPeriod == '2m' && (checkNDDSMissing('1m') || filled2m != 'Yes') ) {
			eformToForwardTo = "NDDS+1%262+Months";
		} else if(latestPeriod == '4m' && checkNDDSMissing('4m') ) {
			eformToForwardTo = "NDDS+4+Months";
		} else if(latestPeriod == '6m' && checkNDDSMissing('6m')) {
			eformToForwardTo = "NDDS+6+Months";
		} else if(latestPeriod == '9m' && checkNDDSMissing('9m')) {
			eformToForwardTo = "NDDS+9+Months";
		} else if(latestPeriod == '12m' && checkNDDSMissing('12m')) {
			eformToForwardTo = "NDDS+12+Months";
		} else if(latestPeriod == '15m' && checkNDDSMissing('15m')) {
			eformToForwardTo = "NDDS+15+Months";
		} else if(latestPeriod == '18m' && checkNDDSMissing('18m')) {
			eformToForwardTo = "NDDS+18+Months";
		} else if(latestPeriod == '2y' && age < 30 && checkNDDSMissing('24m')) {
			eformToForwardTo = "NDDS+24+Months";
		}else if(latestPeriod == '2y' && age >= 30 && age < 36 && checkNDDSMissing('30m')) {
			eformToForwardTo = "NDDS+30+Months";
		}else if(latestPeriod == '2y' && age >= 36 && checkNDDSMissing('3y')) {
			eformToForwardTo = "NDDS+3+Years";
		}else if(latestPeriod == '4y' && age < 60 && checkNDDSMissing('4y')) {
			eformToForwardTo = "NDDS+4+Years";
		}else if(latestPeriod == '4y' && age >= 60 && age < 72 && checkNDDSMissing('5y')) {
			eformToForwardTo = "NDDS+5+Years";
		} else if(latestPeriod == '4y' && age >= 72 && checkNDDSMissing('6y')) {
			eformToForwardTo = "NDDS+6+Years";
		}

		if(eformToForwardTo != null) {
			location.replace("./efmOpenEformByName.do?eform_name="+eformToForwardTo+"&demographic_no="+demoId);
		} else {
			location.replace("./efmOpenEformByName.do?eform_name=Summary+Report:+Well+Baby+Visit&demographic_no="+demoId);
		}
		  
	}
	
	function resumeNonGraphDefaults() {
		document.formRourke.action = default_action;
		document.formRourke.target = "_self";
		statusGraph = false;
		changeWeightsUnit();
	}

	function frms_ready() {
		var frm_titles = ["Rourke Baby Record 18-month visit", "Nipissing District Development Screen"];
		var frm_subjects = [$("subject").value, $("ndds_subject").value];
		var frm_dates = [$("visit_date_18m").value, $("ndds_today").value];

		for (var i=0; i<frm_subjects.length; i++) {
			if (frm_subjects[i].substr(0,9)!="Submitted") {
				if (i==1) {
					$("tickler_assignee_ndds").value = $("patient_mrp").value;
					alert("Please complete the Nipissing District Development Screen form for the baby");
				}
				return false;
			}
			else if (!isValidDate(frm_dates[i])) {
				alert(frm_titles[i]+" date is invalid! Please re-do the form with a valid date.");
				return false;
			}
			else if (i==0) {
				var date = frm_dates[i].split("-");
				var diffY = date[0] - $("birthdayY").value;
				var diffM = date[1] - $("birthdayM").value + diffY*12;
				var diffD = date[2] - $("birthdayD").value;
				if (diffM<18 || (diffM==18 && diffD<0)) {
					alert(frm_titles[i]+" date is <18 month! Please re-do the form and correct it.");
					return false;
				}
			}
		}
		return true;
	}
	
	function set_current_guide_field() {
		$("current_guide").value = current_guide;
	}
	
	var wtFields = new Array("birth_wt","discharge_wt","weight_1w","weight_2w","weight_1m","weight_2m","weight_4m","weight_6m","weight_9m","weight_12m","weight_15m","weight_18m","weight_2y","weight_4y");
	
	function changeWeightsUnit() {
		for (var i=0; i<wtFields.length; i++) {
			$(wtFields[i]).value = $(wtFields[i]+"Show").value; 
		}
		if (statusGraph) {
			for (var i=0; i<wtFields.length; i++) {
				if ($(wtFields[i]).value != "") {
					$(wtFields[i]).value = $(wtFields[i]).value / 1000;
				}
			}
		}
	}
	
	function load_weights() {
		for (var i=0; i<wtFields.length; i++) {
			$(wtFields[i]+"Show").value = $(wtFields[i]).value;
			checkCopyWeight($(wtFields[i]+"Show"));
		}
	}
	
	function getBoyGirl() {
		if ($("genderM").checked) return "Boy";
		else if ($("genderF").checked) return "Girl"; 
	}
	
	function printHtWtGraph() {
		if (!checkMeasurementsVisitDates("htwt")) return false;
		
		statusGraph = true;
		changeWeightsUnit();

		$("__title").value="Baby Growth Graph1";
		$("__template").value="GrowthChartRourke2009"+getBoyGirl()+"s0_24m"; //pdf
		$("__graphType").value="LENGTH";
		$("__numPages").value=2;
		$("__cfgfile1").value="GrowthChartRourke2009"+getBoyGirl()+"s"; //bottom wt table
		$("__cfgfile2").value="GrowthChartRourke2009"+getBoyGirl()+"s3"; //name and birth day
		$("__cfgfile3").value=null;
		$("__cfgfile4").value=null;
		$("__cfgGraphicFile1").value="GrowthChartRourke2009"+getBoyGirl()+"Graphic"; // page 1 - weight
		$("__cfgGraphicFile2").value="GrowthChartRourke2009"+getBoyGirl()+"Graphic2"; // page 1 - length
		$("__cfgGraphicFile11").value="GrowthChartRourke2009"+getBoyGirl()+"Graphic5"; // page 2 - weight
		$("__cfgGraphicFile12").value="GrowthChartRourke2009"+getBoyGirl()+"Graphic6"; // page 2 - length
		
		document.formRourke.action="../eform/efmPrintPDF.do?submit=graph";
		document.formRourke.target="_blank";
		document.formRourke.submit();
	}

	function printHtWtGraph2() {
		if (!checkMeasurementsVisitDates("htwt")) return false;
		
		statusGraph = true;
		changeWeightsUnit();

		$("__title").value="Baby Growth Graph2";
		$("__template").value="GrowthChartRourke2009"+getBoyGirl()+"s2_19"; //file src http://www.hamiltonfht.ca/docs/public/who-growth-charts---2-to-19-years-girls---height-for-age-and-weight-for-age.pdf
		$("__graphType").value="LENGTH";
		$("__numPages").value=1;
		$("__cfgfile1").value="GrowthChartRourke2009"+getBoyGirl()+"s2x";
		$("__cfgfile2").value="GrowthChartRourke2009"+getBoyGirl()+"s3";
		$("__cfgfile3").value=null;
		$("__cfgfile4").value=null;
		$("__cfgGraphicFile1").value="GrowthChartRourke2009"+getBoyGirl()+"Graphic7"; // page 1 - weight
		$("__cfgGraphicFile2").value="GrowthChartRourke2009"+getBoyGirl()+"Graphic8"; // page 1 - length
		$("__cfgGraphicFile11").value=null; // page 2 - weight
		$("__cfgGraphicFile12").value=null; // page 2 - length
		
		document.formRourke.action="../eform/efmPrintPDF.do?submit=graph";
		document.formRourke.target="_blank";
		document.formRourke.submit();
	}
	
	function printHCGraph() {
		if (!checkMeasurementsVisitDates("hc")) return false;
		
		statusGraph = true;
		changeWeightsUnit();

		$("__title").value="Baby Head Circumference";
		$("__template").value="GrowthChartRourke2009"+getBoyGirl()+"spg2";
		$("__graphType").value="HEAD_CIRC";
		$("__numPages").value=1;
		$("__cfgfile1").value="GrowthChartRourke2009"+getBoyGirl()+"s2";
		$("__cfgfile2").value=null;
		$("__cfgfile3").value=null;
		$("__cfgfile4").value=null;
		$("__cfgGraphicFile1").value="GrowthChartRourke2009"+getBoyGirl()+"Graphic3";
		$("__cfgGraphicFile2").value="GrowthChartRourke2009"+getBoyGirl()+"Graphic4";
		$("__cfgGraphicFile11").value=null;
		$("__cfgGraphicFile12").value=null;
		
		document.formRourke.action="../eform/efmPrintPDF.do?submit=graph";
		document.formRourke.target="_blank";
		document.formRourke.submit();
	}
	
	function checkMeasurementsVisitDates(measurement) {
		var missingVisitDates = "";
		for (var i=0; i<periods.length; i++) {
			if (measurement=="htwt") {
				if ($("height_"+periods[i]).value=="" && $("weight_"+periods[i]).value=="") continue;
			} else if (measurement=="hc") {
				if (periods[i]=="4y" || $("headcirc_"+periods[i]).value=="") continue;
			}
			if ($("visit_date_"+periods[i]).value=="") {
				missingVisitDates += "[" + periods[i] + "] ";
			}
		}
		if (missingVisitDates!="") {
			return confirm("One/more visit date missing. The graph will not plot corresponding data point\n"+missingVisitDates);
		}
		return true;
	}

	function checkVisitDateEntry(visitDateBox) {
		if (!isValidDate(visitDateBox.value)) {
			alert("Invalid visit date!");
			setTimeout(function() { visitDateBox.focus(); }, 10);
		}
		
		
	}
	
	var maxDaysInMonth = [31,28,31,30,31,30,31,31,30,31,30,31];
	
	function isValidDate(dateEntry) {
		if (dateEntry.trim()=="") return true;
		
		//Valid date format: yyyy-m-d
		var dateParts = dateEntry.trim().split("-");
		if (dateParts.length!=3) return false;
		if (isNaN(dateParts[0] || isNaN(dateParts[1]) || isNaN(dateParts[2]))) return false;
		
		if (dateParts[0]<1800) return false;
		if (dateParts[1]<1 || dateParts[1]>12) return false;
		
		if ((dateParts[0]%4==0 && dateParts[0]%100>0) || dateParts[0]%400==0 && dateParts[0]%100==0) {
			maxDaysInMonth[1] = 29; //set Feb days for Leap years
		}

		if (dateParts[2]<1 || dateParts[2]>maxDaysInMonth[dateParts[1]]) return false;
		
		return true;
	}
	
	function formPrint(){
		statusGraph = false;
		changeWeightsUnit(); //for printing Rourke form
		window.print();
	}
	
	//keypress events trigger dirty flag
	var needToConfirm = false;
	document.onkeyup=setDirtyFlag;
	document.onkeydown=stopKeyENTER;
	
	function setDirtyFlag() {
		needToConfirm = true;
	}

	function releaseDirtyFlag(){
		needToConfirm = false;
	}
	
	window.onbeforeunload = confirmExit;
	function confirmExit() {
		 if (needToConfirm) {
			 return "You have attempted to leave this page. Changes will be lost if you have not Save/Submit. Are you sure you want to leave?";
		 }
	}

	function stopKeyENTER(event) {
		if (event.keyCode==13) {
			if (event.target.type!="textarea") return false;
		}
	}


function displayDate(e){
var topBoxDate = document.getElementById(e + '_top');
if(topBoxDate != null){
	var visitDate = document.getElementById(e).value;
	if(visitDate==""){
	visitDate = "-";
	}

	topBoxDate.innerHTML = visitDate;
}

}

var visitD = ["1w","2w","1m","2m","4m","6m","9m","12m","15m","18m","2y","4y"];

function displayAllVisitDates(){
//dummy check
if(document.getElementById('visit_date_1w_top')!=null){
	for(var i = 0; i < visitD.length; i++){
		displayDate('visit_date_'+visitD[i]);
	}
}
}

function calcWtLoss(w){
	if(document.getElementById("weight_1wShow_loss")!=null){
		var currentWt = w.value
		var birthWt = document.getElementById('birth_wt').value;
		var weightLoss = null;

		if(birthWt==null || birthWt==""){
			birthWt = document.getElementById('birth_wtShow').value;
		}

		if(w.id.indexOf("Show") == -1){
			weightLoss = document.getElementById(w.id+"Show_loss");
		}else{
			weightLoss = document.getElementById(w.id+"_loss");
		}

		if( parseInt(currentWt) < parseInt(birthWt) ){
			var p = (currentWt - birthWt) / birthWt * 100;
			p = p.toFixed(2);
			p+="%";

			weightLoss.innerHTML = "<font color='red'>" + p + "</font>";

		}else if( currentWt=="" ||  parseInt(currentWt) >= parseInt(birthWt)){
			weightLoss.innerHTML = "";	
		}

	}
}

function showVisitsWtLoss(){
//dummy check 
if(document.getElementById('weight_1wShow_loss')!=null){
	for(var i = 0; i < 6; i++){
		calcWtLoss(document.getElementById('weight_' + visitD[i]));
	}
}
}

function clearAllWtLoss(e){
	if(e.value==""){
		for(var i = 0; i < 6; i++){
			document.getElementById('weight_' + visitD[i] + 'Show_loss').innerHTML="";
		}
	}
}

function viewAllVisits(){
switch_show_all(true);
show_all_guides();
document.getElementById('restore_current_view').style.display='inline';
window.moveTo(0, 0); 
window.resizeTo(screen.width, screen.height);
}


function showReferralList(serviceName) {
	jQuery.get( "../ws/rs/consults/getReferralPathwaysByService?serviceName=" + serviceName, function( data ) {
			var services = [];
			var specs = [];
			
			jQuery('#referral_list_items option').remove();
			
			if(data.services) {
				if (data.services instanceof Array) {
					services = data.services;
				} else {
					var arr = new Array();
					arr[0] = data.services;
					services = arr;
				}
				
				jQuery.each(services, function (i, item) {
				    jQuery('#referral_list_items').append(jQuery('<option>', { 
				        value: 'service_' + item.serviceId,
				        text : item.serviceDesc
				    }));
				});
				
			}
			
			if(data.specialists) {
				if (data.specialists instanceof Array) {
					specs = data.specialists;
				} else {
					var arr = new Array();
					arr[0] = data.specialists;
					specs = arr;
				}		
				
				jQuery.each(specs, function (i, item) {
				    jQuery('#referral_list_items').append(jQuery('<option>', { 
				        value: item.id,
				        text : item.name
				    }));
				});
				
			} else {
				//alert('No specialists found for this service');
			}
		  
			if(specs.length > 0 || services.length >  0) {
				jQuery("#referral_list_service_name").val(serviceName);
				jQuery("#referral_list_box").show();
			}
			
		});
}

function submit_referral() {
	//find the selected item
	var specId = jQuery("#referral_list_items").val();
	var period = jQuery("#referral_source_period").val();
	var demographicNo = $("demographic_no").value;
	
	if(specId.startsWith("service_")) {
		var serviceId = specId.substring(8);
		console.log('serviceId=' + serviceId);
		
		window.open('../oscarEncounter/oscarConsultationRequest/ConsultationFormRequest.jsp?de='+demographicNo + '&serviceId=' + serviceId + '&source=' + period );	
		
		return;
	}
	
	//send a request to find out the URL to load
	jQuery.get( "../ws/rs/consults/getProfessionalSpecialist?specId=" + specId, function( data ) {
		var serviceName = jQuery("#referral_list_service_name").val();
		
		if(data.eformId != null && data.eformId > 0) {
			//load up eform
			window.open('../eform/efmformadd_data.jsp?fid='+data.eformId+'&demographic_no=' + demographicNo + '&service=' + encodeURI(serviceName) + '&specialist=' + specId + '&source=' + period);	
		} else {
			//alert("no eform , just do a consult request");
			//load up consult request..prefill service and specialist
			window.open('../oscarEncounter/oscarConsultationRequest/ConsultationFormRequest.jsp?de='+demographicNo + '&service=' + encodeURI(serviceName) + '&specialist=' + specId + '&source=' + period );	
		}
	});
	
	
	hide_referral_list();
}

function hide_referral_list() {
	if($("referral_list_box")){
		$("referral_list_box").hide();
	}
}