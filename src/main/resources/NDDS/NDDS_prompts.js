var draftCopy = "Draft copy"; //string constant shared by all BORN forms

function checkRourkeSubject() {
	var rbrSubject = $("#rbr_subject").val();
	if(rbrSubject!=null && rbrSubject!="" && rbrSubject!=draftCopy) {
		return true;
	}
	return false;
}

function checkIfRbrVisitDone(period) {
	var period_msg = period;
	if (Array.isArray(period)) {
		period_msg = period[1];
		period = period[0];
	}
	
	//read the hidden value
	var dateVal = $("#rbr_"+period+"_visit_date").val();
	
	if(!checkRourkeSubject() || dateVal==null || dateVal=='') {
		alert('Please complete Rourke Baby Record '+period_msg+' visit');
		return false;
	}
	return true;
}

function runPrompts(period) {
	
	var disableBornPrompts=false;
	$.ajax('../ws/rs/providerService/settings/get',{async:false,method:'GET',dataType:'json',success:function(data){
		if(data != null && data.content != null) {
			if(data.content[0].disableBornPrompts == true) {
				disableBornPrompts=true;
			}
		}
	}});
	
	console.log('property disableBornPrompts=' + disableBornPrompts);
	
	if(disableBornPrompts === true) {
		window.close();
	}
	
	var demoId=$("#demographic_no").val();
	
	if(checkIfRbrVisitDone(period)) {
		location.replace("./efmOpenEformByName.do?eform_name=Summary+Report:+Well+Baby+Visit&demographic_no="+demoId);
	} else {
		location.replace("./efmOpenEformByName.do?eform_name=Rourke+Baby+Record&demographic_no="+demoId);
	}
}

function setupPrompts(formName, period) {
	
	$("#"+formName).submit(function(event) {
		if ($("#subject").val()==draftCopy) return;
		
		$("#subject").val("Submitted " + $("#today").val() + ", creator:  " + $("#frm_creator").val());
		$("#BottomButtons").hide();
		
		event.preventDefault();
		releaseDirtyFlag();
		
		deferred = $.post($("#"+formName).attr('action'), $(this).serialize());
		
		deferred.success(function () {
			runPrompts(period);
		});
		
		deferred.error(function () {
			alert('An error occured while submitting the form');
		});
	});
}

function printResourceSheet(url){
	var foo = window.open(url, "_blank", "toolbar=yes, scrollbars=yes, fullscreen=yes, status=yes");
	foo.moveTo(0,0);
	foo.resizeTo(screen.availWidth,screen.availHeight);
}
